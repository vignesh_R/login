package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods {

	public MyHome() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Leads")
	private WebElement eleLeads;
	public MyLeads clickLeads() {
		//WebElement eleUsername = locateElement("linkText", "CRM/SFA");
		click(eleLeads);
		//LoginPage lp = new LoginPage();
		return new MyLeads();
	}
		}




